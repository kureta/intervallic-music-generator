# coding=utf-8
"""Run this
"""

from collections import deque
from random import choice
import glob
import os

from img.adapters.midi_adapters import parse_midi, MidiWriter
from img.controllers.markov import MarkovChain

# TODO: easy way to select generated parameters (intervals only, intervals and durations, etc.)
parameters = []
initial_events = []
order = 2
markov = MarkovChain(order)

os.chdir("resources/debussy")
musica = []
for file in glob.glob("*.mid"):
    music = parse_midi(file, 0)
    musica.append(music)
    parameter = [i[0] for i in music]
    # parameter = music.events
    # parameter = [(i.interval, i.delta) for i in music.events]
    markov.generate_markov_dictionary(parameter)
os.chdir("..")

reference_music = choice(musica)
generated = [i[0] for i in reference_music][:order]
# generated = reference_music.events[:order]
# generated = [(i.interval, i.delta) for i in reference_music.events][:order]
prev = deque(generated)

for i in range(len(reference_music)-order):
    nex = markov.get_next_value(tuple(prev))

    reference_music[i+order][0] = nex
    # reference_music.events[i+order] = nex
    # reference_music.events[i+order].interval, reference_music.events[i+order].delta = nex

    prev.append(nex)
    prev.popleft()

writer = MidiWriter(reference_music)
writer.generate_midi_file("resources/output.mid")
