# coding=utf-8
"""Testi
"""

from math import ceil
import theano
from opendeep.models import Model, LSTM, GSN

class LSTM_GSN(Model):
    def __init__(self, input_size):
        super(LSTM_GSN, self).__init__()

        gsn_hiddens = 800
        gsn_layers = 2

        # LSTM that takes in 3D sequences and outputs gsn hiddens (3D sequence of them)
        self.input_size = input_size
        self.rnn = LSTM(
            input_size=self.input_size,
            hidden_size=100,
            # needs to output hidden units for odd layers of GSN
            output_size=gsn_hiddens * (ceil(gsn_layers/2.)),
            # output activation same as hidden activation for GSN
            activation='tanh'
        )

        # Create the GSN that will encode the input space
        gsn = GSN(
            input_size=self.input_size,
            hidden_size=gsn_hiddens,
            layers=gsn_layers,
            walkbacks=4,
            visible_activation='sigmoid',
            hidden_activation='tanh'
        )
        # grab the input arguments
        gsn_args = gsn.args.copy()
        # grab the parameters it initialized
        gsn_params = gsn.get_params()

        # Now hook the two up! RNN should output hiddens for GSN into a 3D tensor (1 set for each timestep)
        # Therefore, we need to use scan to create the GSN reconstruction for each timestep given the hiddens
        def step(hiddens, x):
            gsn = GSN(
                inputs_hook=(self.input_size, x),
                hiddens_hook=(gsn_hiddens, hiddens),
                params_hook=(gsn_params),
                **gsn_args
            )
            # return the reconstruction and cost!
            return gsn.get_outputs(), gsn.get_train_cost()

        (outputs, costs), scan_updates = theano.scan(
            fn=lambda h, x: step(h, x),
            sequences=[self.rnn.output, self.rnn.input],
            outputs_info=[None, None]
        )

        self.outputs = outputs

        self.updates = dict()
        self.updates.update(self.rnn.get_updates())
        self.updates.update(scan_updates)

        self.cost = costs.sum()
        self.params = gsn_params + self.rnn.get_params()

    # Model functions necessary for training
    def get_inputs(self):
        return self.rnn.get_inputs()
    def get_params(self):
        return self.params
    def get_train_cost(self):
        return self.cost
    def get_updates(self):
        return self.updates
    def get_outputs(self):
        return self.outputs
