# coding=utf-8
"""This markov
"""
from collections import defaultdict
from random import choice


class MarkovChain:
    """

    :param src_events:
    """

    def __init__(self, order):
        self.order = order
        self.markov_dict = defaultdict(list)

    def generate_markov_dictionary(self, src_events):
        """Selfi
        :param src_events:
        """

        for index, item in enumerate(src_events):
            if index < self.order:
                continue

            if self.order > 1:
                previous_item = tuple(src_events[index - self.order:index])
            else:
                previous_item = src_events[index - 1]

            self.markov_dict[previous_item].append(item)

    def get_next_value(self, prev_value=None):
        """Lolo yapma
        :param prev_value:
        :return:
        """
        if isinstance(self.markov_dict, defaultdict):
            self.markov_dict = dict(self.markov_dict)

        return choice(self.markov_dict.get(prev_value, choice(list(self.markov_dict.keys()))))
