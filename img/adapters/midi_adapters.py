# coding=utf-8
# TODO: add get_num_tracks function

# Standard library imports
from collections import namedtuple, OrderedDict
from fractions import Fraction
from functools import reduce, partial
from itertools import accumulate, tee, chain
from math import modf
import logging

# 3rd party imports
import mido

# Internal imports

NoteEvent = namedtuple('NoteEvent', ['interval', 'velocity', 'time', 'duration'])

# default tempo is 500000 microseconds per quarter note = 120 beats per minute
DEFAULT_TEMPO = 500000
# default resolution is 480 ticks per quarter note
DEFAULT_RESOLUTION = 480
# Default time signature is 4/4 the other two numbers are
# Internal MIDI metronome clock ticks 24 times per quarter note as per MIDI specification
# 24 means the metronome should tick every quarter notes
# 8 is the number of 32nd notes per one quarter note
DEFAULT_TIME_SIGNATURE = (4, 4, 24, 8)

logger = logging.getLogger(__name__)


def assoc(_d, key, value):
    # return a modified object or dictionary
    from copy import deepcopy
    d = deepcopy(_d)
    if isinstance(d, dict):
        d[key] = value
    else:
        d.__dict__[key] = value
    return d


def compose(data, functions):
    # compose a list of functions
    return reduce(lambda a, x: x(a), functions, data)


def get_time_signature(track):
    time_signature_changes = list(filter(lambda x: x.type == 'time_signature', track))
    if len(time_signature_changes) == 1:
        time_signature = (time_signature_changes[0].numerator,
                          time_signature_changes[0].denominator,
                          time_signature_changes[0].clocks_per_click,
                          time_signature_changes[0].notated_32nd_notes_per_beat)

    elif len(time_signature_changes) == 0:
        time_signature = DEFAULT_TIME_SIGNATURE
    else:
        logger.warning("\t\tThe track has multiple time signature events!!!")
        # TODO: handle time signature changes URGENT!!!
        time_signature = (time_signature_changes[0].numerator,
                          time_signature_changes[0].denominator,
                          time_signature_changes[0].clocks_per_click,
                          time_signature_changes[0].notated_32nd_notes_per_beat)
    return time_signature


def get_bar_length(time_signature, ticks_per_beat):
    return round((ticks_per_beat * 4) * (time_signature[0] / time_signature[1]))


def delta_to_absolute(track):
    def set_time(event, time):
        return assoc(event, 'time', time)

    abs_times = accumulate((x.time for x in track))
    return map(set_time, track, abs_times)


def ticks_to_bars(track, bar_length):
    def t_to_b(note):
        return assoc(note, 'time', note.time / bar_length)

    return map(t_to_b, track)


def filter_out_notes(track):
    return filter(lambda x: x.type == 'note_on' or x.type == 'note_off', track)


def ons_and_offs(val):
    # this function is coupled with notes_with_durations
    # here we sort events on notes so we can couple on and off events of the same note below
    def split(a, x):
        if x.velocity > 0 and x.type == 'note_on':
            return a[0] + [x], a[1]
        elif x.velocity == 0 or x.type == 'note_off':
            return a[0], a[1] + [x]
        else:
            return a

    ons, offs = reduce(split, val, ([], []))
    ons.sort(key=lambda x: (x.note, x.time))
    offs.sort(key=lambda x: (x.note, x.time))
    return ons, offs


def notes_with_durations(ons_offs):
    # this function depends on incoming ons_off being sorted on notes
    # this way it couples on and off events of a single note
    # at the end it sorts the list of events on time
    def note_dur(on, off):
        result = assoc(on, 'duration', off.time - on.time)
        return result

    notes = map(note_dur, ons_offs[0], ons_offs[1])
    return sorted(notes, key=lambda x: (x.time, x.note))


def differences(seq):
    iterable, copied = tee(seq)
    next(copied)
    for x, y in zip(iterable, copied):
        yield y - x


def notes_with_intervals(notes):
    # prepend 0 interval at the beginning
    def set_interval(note, inter):
        return assoc(note, 'interval', inter)

    result = map(set_interval, notes, chain([0], differences((x.note for x in notes))))
    return result


def format_notes(notes):
    # format the results of all previous calculations into a named tuple
    # convert time and duration to Fractions.
    # TODO: unhandled cases: a duration is longer than one bar
    # TODO: unhandled cases: a note spans more than one bars
    # TODO: time between two events is longer than one bar
    def format_one_note(note):
        note = NoteEvent(interval=note.interval, velocity=note.velocity,
                         time=note.time,
                         duration=Fraction(note.duration).limit_denominator(64))
        return note

    result = map(format_one_note, notes)
    return list(result)


def partition(domain, relation):
    classes = OrderedDict()
    for x in domain:
        for representative, values in classes.items():
            if relation(x, representative):
                classes[representative].append(x)
                break
        else:
            classes[x] = [x]
    return list(classes.values())


def split_into_bars(notes):
    def in_same_bar(x, y):
        return modf(x.time)[1] == modf(y.time)[1]

    return partition(notes, in_same_bar)


def remove_int_of_times(bars):
    def int_of_fraction(number):
        frac = Fraction(number).limit_denominator(64)
        num = frac.numerator
        denom = frac.denominator

        return Fraction(num % denom, denom)

    def strip_int(note):
        return NoteEvent(interval=note.interval, velocity=note.velocity,
                         time=int_of_fraction(note.time),
                         duration=note.duration)

    # result is list of bars, bars are lists of notes
    result = []
    for bar in bars:
        result.append(list(map(strip_int, bar)))
    return result


def parse_midi(file_name, track_no=0, merge_all=False):
    midi_file = mido.MidiFile(file_name)
    track = midi_file.tracks[track_no]

    time_signature = get_time_signature(track)
    bar_length = get_bar_length(time_signature, midi_file.ticks_per_beat)
    initial_tempo = first_tempo(track)

    if merge_all:
        midi_in = mido.MidiTrack()
        for t in midi_file.tracks:
            midi_in += delta_to_absolute(t)
    else:
        midi_in = delta_to_absolute(track)

    bars = compose(midi_in, [
        partial(ticks_to_bars, bar_length=bar_length),
        filter_out_notes,
        ons_and_offs,
        notes_with_durations])

    initial_pitch = bars[0].note

    bars = compose(bars, [
        notes_with_intervals,
        format_notes,
        split_into_bars,
        remove_int_of_times
    ])

    logger.info("\t\tTrack contains {} bars. Initial pitch is {}.".format(len(bars), initial_pitch))

    music = {'event': bars,
             'initial_pitch': initial_pitch,
             'initial_tempo': initial_tempo,
             'bar_length': bar_length,
             'time_signature': time_signature,
             'ticks_per_beat': midi_file.ticks_per_beat}

    return music


# Intervals, velocities, times, and durations are characters.
# Bars are sentences. Notes are words. So we can use a text generator LSTM.


def filter_tempos(track):
    return filter(lambda x: x.type == 'set_tempo', track)


def first_tempo(track):
    tempos = filter_tempos(track)

    try:
        return next(tempos).tempo
    except StopIteration:
        return DEFAULT_TEMPO


# =================================================================================================
# =========================================== Midi Writer =========================================
# =================================================================================================

def add_int_to_times(bars):
    new_track = []
    for index, bar in enumerate(bars):
        for note in bar:
            new_track.append(NoteEvent(interval=note.interval, velocity=note.velocity,
                                       time=float(note.time) + float(index),
                                       duration=float(note.duration) + float(note.time) + float(index)))
    return new_track


def interval_to_pitch(track, initial_pitch):
    new_track = []
    carry = initial_pitch
    for note in track:
        carry += note.interval
        if carry > 127:
            carry -= 60
        elif carry < 0:
            carry += 60
        new_track.append(NoteEvent(interval=carry, velocity=note.velocity,
                                   time=note.time,
                                   duration=note.duration))
    return new_track


def bars_to_ticks(track, bar_length):
    def t_to_b(note):
        return NoteEvent(interval=note.interval, velocity=note.velocity,
                         time=round(note.time * bar_length),
                         duration=round(note.duration * bar_length))

    return map(t_to_b, track)


def split_ons_offs(track):
    ons = []
    offs = []

    for note in track:
        ons.append(NoteEvent(interval=note.interval, velocity=note.velocity,
                             time=note.time,
                             duration=0))
        offs.append(NoteEvent(interval=note.interval, velocity=note.velocity,
                              time=note.duration,
                              duration=0))

    return ons, offs


def abs_to_delta(notes):
    def set_time(note, time):
        return assoc(note, 'time', time)

    result = map(set_time, notes, differences(chain([0], (x.time for x in notes))))
    return result


def write_midi(music, file_path):
    # TODO: make parse_midi utility functions more general and usable with write midi
    note_events = music['event']

    track = add_int_to_times(note_events)
    track = interval_to_pitch(track, music['initial_pitch'])
    track = bars_to_ticks(track, music['bar_length'])
    ons, offs = split_ons_offs(track)

    midi_events = list()

    for note_on in ons:
        midi_events.append(mido.Message('note_on', note=note_on.interval, velocity=note_on.velocity, time=note_on.time))
    for note_off in offs:
        midi_events.append(mido.Message('note_off', note=note_off.interval, velocity=0, time=note_off.time))

    midi_events.sort(key=lambda x: x.time)
    midi_events = abs_to_delta(midi_events)

    with mido.MidiFile() as mid:
        mid.ticks_per_beat = music['ticks_per_beat']
        midi_track = mido.MidiTrack()
        mid.tracks.append(midi_track)

        midi_track.append(mido.MetaMessage('time_signature', numerator=music['time_signature'][0],
                                           denominator=music['time_signature'][1],
                                           clocks_per_click=music['time_signature'][2],
                                           notated_32nd_notes_per_beat=music['time_signature'][3],
                                           time=0))
        midi_track.append(mido.MetaMessage('set_tempo', tempo=music['initial_tempo'], time=0))

        for event in midi_events:
            midi_track.append(event)

        mid.save(file_path)
