# coding=utf-8
"""Mesti
"""


class NeuralAdaptor:
    """Neural adaptor ignores initial event
    :param music_list:
    """

    def __init__(self, music_list):
        self.max_length = 0
        self.all_seqs = []

        for music in music_list:
            one_seq = []
            for event in music:
                one_seq.append([event.interval, event.delta, event.duration, event.velocity])
            if len(one_seq) > self.max_length:
                self.max_length = len(one_seq)
            self.all_seqs.append(one_seq)

        for seq in self.all_seqs:
            while len(seq) < self.max_length:
                seq.append([0, 0, 0, 0])
