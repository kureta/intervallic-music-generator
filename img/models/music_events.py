# coding=utf-8
""" This module contains all the required data types
"""

# TODO: remove this model and use dict instead


class Event:
    def __init__(self, interval, duration, delta, velocity):
        self.interval = interval
        self.duration = duration
        self.delta = delta
        self.velocity = velocity

    def __repr__(self):
        return "{{interval: {}, duration: {}, delta: {}, velocity: {}}}".format(self.interval, self.duration,
                                                                                self.delta, self.velocity)


class MusicEvents:
    """ MusicEvents Model

    :param int initial_event:
    :param list events:
    """

    def __init__(self, initial_event, events):
        self.initial_event = initial_event
        self.events = events
