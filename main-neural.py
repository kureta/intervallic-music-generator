# coding=utf-8
"""Run this
"""

import os
from opendeep import config_root_logger
from opendeep.data import NumpyDataset
from opendeep.optimization import RMSProp
from img.adapters.midi_adapters import MidiParser
from img.adapters.neural_adapters import NeuralAdaptor
from img.controllers.lstm_gsn import LSTM_GSN

directory = "JSB Chorales"

music_list = []

for a_file in os.listdir(directory):
    if a_file.endswith(".mid"):
        parser = MidiParser(directory+'/'+a_file)
        music_list.append(parser.parse_midi(0).events)

config_root_logger()

na = NeuralAdaptor(music_list)
dataset = NumpyDataset(train_inputs=na.all_seqs)
model = LSTM_GSN(na.max_length)

opti = RMSProp(dataset, model)
opti.train()
