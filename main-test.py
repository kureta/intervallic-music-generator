# coding=utf-8
"""Run this
"""
import logging
import os
import pickle

from keras.models import Sequential
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM

import numpy as np
import random

from img.adapters.midi_adapters import parse_midi, write_midi, NoteEvent

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

pickle_file = 'resources/parsed_bach.pickle'

directory = 'resources/bach/4-voice/'
musics = []
data = []

if not os.path.exists(pickle_file):
    logger.info('Parsing midi files...')
    for a_file in os.listdir(directory):
        if a_file.endswith(".MID"):
            music = parse_midi(directory + a_file, merge_all=True)
            musics.append(music)

    logger.info('Saving parsed midi data...')
    with open(pickle_file, 'wb') as handle:
        pickle.dump(musics, handle, pickle.HIGHEST_PROTOCOL)

else:
    logger.info('Loading saved parsed midi data...')
    with open(pickle_file, 'rb') as handle:
        musics = pickle.load(handle)

first_music = parse_midi(directory + '000206B_.MID', merge_all=True)

logger.info('Reshaping parsed midi data for the neural network...')
for music in musics:
    for bar in music['event']:
        for note in bar:
            data.append((note.interval, note.time, note.duration))
            # data.append(note.interval)
            # data.append(note.time)
            # data.append(note.duration)
        data.append('bar')

print('corpus length:', len(data))

chars = set(data)
print('total chars:', len(chars))
char_indices = dict((c, i) for i, c in enumerate(chars))
indices_char = dict((i, c) for i, c in enumerate(chars))

# cut the text in semi-redundant sequences of maxlen characters
maxlen = 20
step = 3
sentences = []
next_chars = []
for i in range(0, len(data) - maxlen, step):
    sentences.append(data[i: i + maxlen])
    next_chars.append(data[i + maxlen])
print('nb sequences:', len(sentences))

print('Vectorization...')
X = np.zeros((len(sentences), maxlen, len(chars)), dtype=np.bool)
y = np.zeros((len(sentences), len(chars)), dtype=np.bool)
for i, sentence in enumerate(sentences):
    for t, char in enumerate(sentence):
        X[i, t, char_indices[char]] = 1
    y[i, char_indices[next_chars[i]]] = 1

# build the model: 2 stacked LSTM
print('Build model...')
model = Sequential()
model.add(LSTM(512, return_sequences=True, input_shape=(maxlen, len(chars))))
model.add(Dropout(0.2))
model.add(LSTM(512, return_sequences=False))
model.add(Dropout(0.2))
model.add(Dense(len(chars)))
model.add(Activation('softmax'))

model.compile(loss='categorical_crossentropy', optimizer='rmsprop')

weights_file = 'resources/model_weights.h5'
if os.path.exists(weights_file):
    model.load_weights(weights_file)


def sample(a, temperature=1.0):
    # helper function to sample an index from a probability array
    a = np.log(a) / temperature
    a = np.exp(a) / np.sum(np.exp(a))
    return np.argmax(np.random.multinomial(1, a, 1))


# train the model, output generated text after each iteration
for iteration in range(1, 60):
    print()
    print('-' * 50)
    print('Iteration', iteration)
    model.fit(X, y, batch_size=128, nb_epoch=1)

    start_index = random.randint(0, len(data) - maxlen - 1)

    for diversity in [0.2, 0.5, 1.0, 1.2]:
        print()
        print('----- diversity:', diversity)

        generated = []
        sentence = data[start_index: start_index + maxlen]
        generated += sentence
        print('----- Generating with seed: "{}"'.format(sentence))
        # sys.stdout.write(generated)

        for i in range(400):
            x = np.zeros((1, maxlen, len(chars)))
            for t, char in enumerate(sentence):
                x[0, t, char_indices[char]] = 1.

            preds = model.predict(x, verbose=0)[0]
            next_index = sample(preds, diversity)
            next_char = indices_char[next_index]

            generated += [next_char]
            sentence = sentence[1:] + [next_char]

            # sys.stdout.write(next_char)
            # sys.stdout.flush()
            # print("{}".format(next_char))
        print()
        print("{}".format(generated))

        bars = []
        notes = []
        for event in generated:
            if event == 'song' or event == 'bar':
                bars.append(notes)
                notes = []
            else:
                notes.append(NoteEvent(interval=event[0],
                                       time=event[1],
                                       duration=event[2],
                                       velocity=64))
        first_music['event'] = bars
        write_midi(first_music, 'outputs/{}-{}.mid'.format(iteration, diversity))

    model.save_weights(weights_file, overwrite=True)
